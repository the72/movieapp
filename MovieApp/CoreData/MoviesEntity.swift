//
//  MoviesEntity.swift
//  MovieApp
//
//  Created by Nurba on 21.05.2021.
//

import Foundation
import CoreData

class MoviesEntity: NSManagedObject{
    
   static func findMovie(with id: Int, context: NSManagedObjectContext) -> MoviesEntity?{
        let requestResult: NSFetchRequest<MoviesEntity> = MoviesEntity.fetchRequest()
        requestResult.predicate = NSPredicate(format: "id == %d", id)
        
        do {
            let movies = try context.fetch(requestResult)
            if movies.count > 0{
                assert(movies.count == 1, "Duplicate was found in DB!!")
                return movies[0]
            }
        }catch{
            print(error)
        }
        return nil
    }
}
