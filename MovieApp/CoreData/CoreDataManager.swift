//
//  CoreDataManager.swift
//  MovieApp
//
//  Created by Nurba on 21.05.2021.
//

import Foundation
import CoreData

class CoreDataManager {
    static let shared = CoreDataManager()
    
 lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "LocalDBModel")
        container.loadPersistentStores(completionHandler: {
            (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    private init() {}
    
    func allMovie() -> [MovieEntity.Movie] {
        let context = persistentContainer.viewContext
        let request: NSFetchRequest<MoviesEntity> = MoviesEntity.fetchRequest()
        
        let movies = try? context.fetch(request)
        
        return movies?.map({ MovieEntity.Movie(from: $0 )}) ?? []
    }
  
    
    func addMovie(_ movie: MovieEntity.Movie){
        let context = persistentContainer.viewContext
        context.perform {
         
            let newMovie = MoviesEntity(context: context)
            newMovie.id = Int64(movie.id)
            newMovie.title = movie.title
         
            
            
        }
        save()
    }
    func addMovie(_ movie: Detail){
        let context = persistentContainer.viewContext
        context.perform {
            if let id = movie.id{
            let newMovie = MoviesEntity(context: context)
            newMovie.id = Int64(id)
            newMovie.title = movie.original_title
     
            
            }
        }
        save()
    }
    
    func save(){
        let context = persistentContainer.viewContext
        
        do{
           try context.save()
        }catch {
            print(error)
        }
    }
    
    func delete(with id: Int){
      let  context = persistentContainer.viewContext
        if let movie = MoviesEntity.findMovie(with: id, context: context){
            context.delete(movie)
            save()
        }
    }
}
