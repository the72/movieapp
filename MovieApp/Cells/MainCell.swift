

import UIKit
import Kingfisher

class MainCell: UITableViewCell {
    
    public static let identifier: String = "MainCell"

    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var desclabel: UILabel!
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var ratindView: UIView!
    @IBOutlet weak var favoriteButton: UIButton!
    
    private let context = CoreDataManager.shared.persistentContainer.viewContext
    private let coreDataManager = CoreDataManager.shared
    
    public var movie: MovieEntity.Movie? {
        didSet{
            if let movie = movie {
                let posterURL = URL(string:"https://image.tmdb.org/t/p/w500" + (movie.poster ?? ""))
                posterImage.kf.setImage(with: posterURL)
                titleLabel.text = movie.title
                desclabel.text = movie.releaseData
                rating.text = "\(String(describing: movie.rating))"
                
                if let _ = MoviesEntity.findMovie(with: movie.id, context: context){
                    favoriteButton.setImage(UIImage(named: "starFill"), for: .normal)
                    }else{
                        favoriteButton.setImage(UIImage(named: "star"), for: .normal)
                    

                }
                
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        posterImage.layer.cornerRadius = 12
        posterImage.layer.masksToBounds = true
        ratindView.layer.cornerRadius = 20
        ratindView.layer.masksToBounds = true
        
    }
    @IBAction func favoriteButtonPressed(_ sender: Any) {
        
     
        
        if let movie = movie{
            if let _ = MoviesEntity.findMovie(with: movie.id, context: context){
                favoriteButton.setImage(UIImage(named: "star"), for: .normal)
                coreDataManager.delete(with: movie.id)
            }else{
         
                    favoriteButton.setImage(UIImage(named: "starFill"), for: .normal)
                    coreDataManager.addMovie(movie)
                
               
            }

        }

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
